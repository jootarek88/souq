<?php require_once('header-admin.php')?>

    <div class="row">
    <div class="col-md-3">
    <form action='categories-action.php' method='post' enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleInputEmail1">category name</label>
    <input type="text" class="form-control" name='name' placeholder="Enter brand name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">category img </label>
    <input type="file" class="form-control" name='category_img' placeholder="Enter brand name">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    </div>

    <div class="col-md-8">
    <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">category</th>
      <th scope="col">category img</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  <?php 
   require_once("classes/categories.php");
    $categories =  categories::get_all();
    foreach($categories as $category){ 
  ?>
    <tr>
      <th scope="row"><?php echo $category['id']?></th>
      <td><?php echo $category['name']?></td>
      <td><img src="<?php echo $category['category_img']?>" alt=""></td>
      <td><a href="">Edit</td>
      <td><a href="categories-action.php?id=<?php echo $category['id']?>">Delete</td>

      
    </tr>
    <?php }?>
  </tbody>
</table>

    </div>
    </div>










<?php require_once('footer-admin.php')?>