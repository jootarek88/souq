<?php require_once('header-admin.php')?>

<div class="row">
    <div class="col-md-5 m-auto">
        <form action='products-action.php' method='post' enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">product name</label>
                <input type="text" class="form-control" name='name' placeholder="Enter product name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">product image</label>
                <input type="file" class="form-control" name='product_images' placeholder="Enter product name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">description</label>
                <textarea  id="froala-editor" type="text" class="form-control" name='description'
                    placeholder="Enter description"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">barnd name</label>
                <input type="text" class="form-control" name='unit_price' placeholder="Enter unit price">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">barnd name</label>
                <input type="text" class="form-control" name='shipping_price' placeholder="Enter shipping_price">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">barnd name</label>
                <input type="number" class="form-control" name='qty'>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">barnd name</label>
                <select name="status" class="form-control">
                    <option>Available</option>
                    <option>Not Available</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">vendor name</label>
                <select name='vendor_id' class="form-control">
                    <?php
    require_once("classes/users.php");
    $users = users::get_all();
    foreach($users as $user)
    {
      echo "<option value='".$user['id']."'>".$user['full_name']."</option>";
    }
    ?>

                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">sub-categories name</label>
                <select name='sub_category_id' class="form-control">
                    <?php
    require_once("classes/sub-categories.php");
    $scats = sub_categories::get_all();
    foreach($scats as $scat)
    {
      echo "<option value='".$scat['id']."'>".$scat['sub_cat_name']."</option>";
    }
    ?>

                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">categories name</label>
                <select name='category_id' class="form-control">
                    <?php
    require_once("classes/categories.php");
    $cats = categories::get_all();
    foreach($cats as $cat)
    {
      echo "<option value='".$cat['id']."'>".$cat['name']."</option>";
    }
    ?>

                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">barnd name</label>
                <select name='brand_id' class="form-control">
                    <?php
                require_once("classes/brands.php");
                $brands = brands::get_all();
                foreach($brands as $brand)
                {
                    echo "<option value='".$brand['id']."'>".$brand['name']."</option>";
                }
                ?>

                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <div class="col-md-12">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">name</th>
                    <th scope="col">image</th>
                    <th scope="col">description</th>
                    <th scope="col">unit_price</th>
                    <th scope="col">shipping_price</th>
                    <th scope="col">qty</th>
                    <th scope="col">status</th>
                    <th scope="col">vendor_id</th>
                    <th scope="col">sub_category_id</th>
                    <th scope="col">category_id</th>
                    <th scope="col">create_at</th>
                    <th scope="col">brand_id</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php 
   require_once("classes/products.php");
    $products =  products::get_all();
    $_SESSION['products']=$products;
    foreach($_SESSION['products'] as $key => $product){ 
  ?>
                <tr>
                    <th scope="row"><?php echo $product['id']?></th>
                    <td><?php echo $product['name']?></td>
                    <td><img src="<?php echo $product['product_images']?>" alt=""></td>
                    <td><?php echo $product['description']?></td>
                    <td><?php echo $product['unit_price']?></td>
                    <td><?php echo $product['shipping_price']?></td>
                    <td><?php echo $product['qty']?></td>
                    <td><?php echo $product['status']?></td>
                    <td><?php echo $product['vendor_id']?></td>
                    <td><?php echo $product['sub_category_id']?></td>
                    <td><?php echo $product['category_id']?></td>
                    <td><?php echo $product['create_at']?></td>
                    <td><?php echo $product['brand_id']?></td>
                    <td><a href="products-action.php?id=<?php echo $product['id']?>">Edit</td>
                    <td><a href="products-action.php?id=<?php echo $product['id']?>">Delete</td>


                </tr>
                <?php }?>
            </tbody>
        </table>

    </div>
</div>










<?php require_once('footer-admin.php')?>