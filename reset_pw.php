<DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">

        <link rel="stylesheet" href="css/style.css">


    </head>

    <body>
     
        <div class="container-site">


            <section class="site1-one">

                <nav class="navbar navbar-expand-lg ">
                    <div class="collapse-w3">
                        <div class="w3-sidebar w3-bar-block w3-collapse w3-card" style="width:200px;" id="mySidebar">
                            <button class="w3-bar-item w3-button w3-hide-large" onclick="w3_close()">Close
                                &times;</button>
                            <div class="w3-sidebar w3-bar-block">
                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 2</a>
                                <div class="w3-dropdown-hover">
                                    <button class="w3-button">COMUTERS & LAPTOPS <i
                                            class="fa fa-caret-down"></i></button>
                                    <div class="w3-dropdown-content w3-bar-block">
                                        <ul>
                                            <li><a href="#">Laptops,Ultrabooks & Computers</a></li>
                                            <li><a href="#">camiras & photography</a></li>
                                            <li><a href="#">smart phones & Tablets</a></li>
                                            <li><a href="#">Video Games & Consoles</a></li>
                                            <li><a href="#">Tv & Audio</a></li>
                                            <li><a href="#">Gadgets</a></li>
                                            <li><a href="#">Car Elctronic & GPS</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="#" class="w3-bar-item w3-button">Link 3</a>
                            </div>
                        </div>

                        <button class="w3-button " onclick="w3_open()">&#9776;</button>
                    </div>
                    <a class="navbar-brand mr-auto" href="#"><img src="images/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="search-mob collapse-w3">
                        <button id="search-btn"><i class="fas fa-search"></i></button>
                        <div class="search position-absolute " id="search">
                            <form style="display: flex;">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter email">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>

                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item dropdown" style="margin: auto;">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <div class="w-25">
                            <ul class="navbar-nav mr-auto d-flex justify-content-around align-items-center">
                                <li class="nav-item d-flex justify-content-around align-items-center">
                                    <i class="fas fa-retweet"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="far fa-heart"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item">
                                    <span class="d-block">your Cart</span>
                                    <span>$15410</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>


        </div>

        <div class="container-site">

            <section class="sec-login">

                    <div class="col-lg-5 m-auto" >
                        <div class="title">
                            <h2>Rest Admin Password Account</h2>
                            <hr>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Libero dolorem consequatur ut
                                mollitia ab!</p>
                        </div>
                        <form method="post" action='reset_pw_action.php'>
                        <input type="hidden"    name="user_id" value="<?php echo $_GET['user_id']?>"?>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <input type="password" name='password' class="form-control" placeholder="Enter password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Confirem Password</label>
                                <input type="password" name='password2' class="form-control" id="exampleInputPassword1"
                                    placeholder="re-Password">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
              
                    </div>
                    
                 
                </div>

            </section>


        </div>









        <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="js/aos.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/bootstrap.js"></script>

        <script src="js/js.js"></script>
    </body>

    </html>