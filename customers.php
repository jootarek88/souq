<?php
$page_name = "customers";

require_once('header-admin.php');

?>
<?php 
    require_once('classes/customers.php');
    $full_name="";
    if(!empty($_GET['full_name'])){
        $full_name = $_GET['full_name'];
        $customers = customers::search($full_name);

    }else{
        $customers =  customers::get_all();
    }
   
    ?>



 <div class="card border-secondary">
        <div class="card-header bg-secondary text-light">
           All Categories 
           <div class="float-right">
          <form class="form-header" method="get">

            <input class="form-control form-control-sm border-light"  type="text" name="full_name" value="<?php echo  $full_name ;?>" placeholder="Search Category" />
            <button class="btn btn-dark " type="submit">
                <i class="zmdi zmdi-search"></i>
            </button>
        </form> 
           </div>
        </div>
        <div class="card-body" style="overflow: auto">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>id</td>                        
                        <td>email</td>                        
                        <td>Name</td>                        
                        <td>mobile</td>                        
                        <td>Date</td>                        
                        <td>delete</td>                        
                    </tr>
                </thead>
                <tbody>
                <?php

                    foreach($customers as $customer)       
                    {
                 ?>
                    <tr>
                        <td><?php echo $customer["id"] ?></td>
                        <td><?php echo $customer["email"] ?></td>
                        <td><?php echo $customer["Full_Name"] ?></td>
                        <td><?php echo $customer["mobile"] ?></td>
                        <td><?php echo $customer["create_at"] ?></td>
                        <td><a href="http://"></a></td>

                    </tr>
                    <?php
                     }
                    ?>
                </tbody>
            </table>
        </div>
        
        </div>
    </div>
</div>











<?php
require_once('footer-admin.php');?>