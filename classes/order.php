<?php

class orders{
    var $id;
    var $create_at;
    var $customer_id;
    var $customer_contact_id;
    var $status;
    var $ordersscol;


    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into orders(create_at,customer_id,customer_contact_id,status,ordersscol) values(:ca,:c_id,:c_id_d,:s,:oc)");
        $set->bindParam(":ca" , $this->create_at);
        $set->bindParam(":c_id" , $this->customer_id);
        $set->bindParam(":oc" , $this->customer_contact_id);
        $set->bindParam(":c_id_d" , $this->status);
        $set->bindParam(":oc" , $this->ordersscol);
        $set->execute();

        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from orders');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

   static function rendom_select(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from orders  by rand() limit 5');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
       $db->exec("delete from orders where id =".$this->id);
        $db = null;

    }
    
    
}