<?php

class categories{
    var $c_id;
    var $name;
    var $category_img;


    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into categories(name,category_img) values(:n,:img)");
        $set->bindParam(":n" , $this->name);
        $set->bindParam(":img" , $this->category_img);

        $set->execute();

        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from categories');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

   static function rendom_select(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from categories order by rand() limit 5');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
       $db->exec("delete from categories where id =".$this->c_id);
        $db = null;

    }
    
    
}