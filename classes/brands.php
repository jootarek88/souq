<?php

class brands{
    var $id;
    var $name;
    var $site;
    var $logo;


    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into brands(name ,site, logo) values(:n , :s , :l)");
        $set->bindParam(":n" , $this->name);
        $set->bindParam(":s" , $this->site);
        $set->bindParam(":l" , $this->logo);
        $set->execute();

        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select id , name,site,logo from brands');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }
    
   static  function find_by_id($id){

    require_once('./config.php');
    $db = new PDO(DSN, USER_NAME , PASSWORD);
    $stm = $db->query('select id , name from brands where id='.$id);
    $rlt=$stm->fetchAll();

    $db = null;
            return $rlt;

}

    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
       $db->exec("delete from brands where id =".$this->id);
        $db = null;

    }
    
    
}