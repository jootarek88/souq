<?php

class sub_categories{
    var $s_id;
    var $sub_cat_name;
    var $category_id;



    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into sub_categories(sub_cat_name,category_id) values(:n,(select id from categories where id = ".$this->category_id.")) ");
        $set->bindParam(":n" , $this->sub_cat_name);
        $set->execute();

        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select id ,sub_cat_name,category_id from sub_categories');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

   static  function find_by_id($id){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select id ,sub_cat_name from sub_categories where category_id='.$id);
        $rlt=$stm->fetchAll();

        $db = null;
                return $rlt;

    }


    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
       $db->exec("delete from sub_categories where id =".$this->s_id);
        $db = null;

    }
    
    
}