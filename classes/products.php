<?php

class products{
    var $id;
    var $name;
    var $description;
    var $unit_price;
    var $shipping_price;
    var $qty;
    var $status;
    var $vendor_id;
    var $sub_category_id;
    var $category_id;
    var $create_at;
    var $brand_id;
    var $product_images;



    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into products(name,description,unit_price,shipping_price,qty,status,vendor_id,sub_category_id,category_id,create_at,brand_id,product_images) values(:n,:des,:u_p,:s_p,:q,:st,(select id from users where id = ".$this->vendor_id."),(select id from sub_categories where id = ".$this->sub_category_id."),(select id from categories where id = ".$this->category_id."),:tm,(select id from brands where id = ".$this->brand_id."),:p_img) ");
        $set->bindParam(":n" , $this->name);
        $set->bindParam(":des" , $this->description);
        $set->bindParam(":u_p" , $this->unit_price);
        $set->bindParam(":s_p" , $this->shipping_price);
        $set->bindParam(":q" , $this->qty);
        $set->bindParam(":st" , $this->status);
        $set->bindParam(":tm" , $this->create_at);
        $set->bindParam(":p_img" , $this->product_images);
        $set->execute();

        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from products');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

    function update(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME,PASSWORD);
       $set = $db->prepare('update products set qty=:q  where id=:id');
        $set->bindParam(":p" , $this->password);
        $set->bindParam(":id" , $this->id);
        $set->execute();
        $db = null;
    }

   static  function find_by_id($sc_id){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select* from products where sub_category_id='.$sc_id);
        $rlt=$stm->fetchAll();

        $db = null;
                return $rlt;

    }

    function edit()
    {
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $db->exec("update products set name='".$this->name.", description='".$this->description.",unit_price='".$this->unit_price.",shipping_price='".$this->shipping_price.",qty='".$this->qty.",status='".$this->status.",status='".$this->status.",vendor_id='".$this->vendor_id.",sub_category_id='".$this->sub_category_id.",category_id='".$this->category_id.",brand_id='".$this->brand_id.",product_images='".$this->product_images."' where id=" .$this->id );
        //close
        $db = null;
    }
   static  function get_by_session($idp){
        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->prepare('select* from products where id=?');
        $stm->execute([$idp]);
      
        return $stm->fetchObject();
        $db = null;
    }
    static function rendom_select(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select * from products order by rand() limit 5');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
       $db->exec("delete from products where id =".$this->id);
        $db = null;

    }
    
    
}