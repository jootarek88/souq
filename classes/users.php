<?php

class users{
    var $id;
    var $email;
    var $password;
    var $type;
    var $full_name;
    var $mobile;
    var $status;
    var $create_at;
    var $created_by;


    function add(){
        require_once("./config.php");
        // connection
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $set = $db->prepare("insert into users(email ,password, type, full_name, mobile, status , create_at, created_by) values(:e , :p , :t, :f , :m , :s , :c_a , :c_b)");
        $set->bindParam(":e" , $this->email);
        $set->bindParam(":p" , $this->password);
        $set->bindParam(":t" , $this->type);
        $set->bindParam(":f" , $this->full_name);
        $set->bindParam(":m" , $this->mobile);
        $set->bindParam(":s" , $this->status);
        $set->bindParam(":c_a" , $this->create_at);
        $set->bindParam(":c_b" , $this->created_by);
        $set->execute();
        $this->id =$db->lastInsertId();
        //close
        $db = null;

    }

   static function get_all(){

        require_once('./config.php');
        $db = new PDO(DSN, USER_NAME , PASSWORD);
        $stm = $db->query('select id ,email ,password, type, full_name, mobile, status , create_at, created_by from users');
        $rlt=$stm->fetchAll();
        $db = null;
        return $rlt;
    }

    function delete(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME , PASSWORD);
        $db->exec("update users set status = 'deactive'  where id=:id ");
        $db = null;

    }

    function active(){
        require_once('./config.php');
        $db = new PDO(DSN,USER_NAME,PASSWORD);
       $set = $db->prepare('update users set password=:p , status="active" where id=:id');
        $set->bindParam(":p" , $this->password);
        $set->bindParam(":id" , $this->id);

        $set->execute();
        $db = null;

    }
    
    
}