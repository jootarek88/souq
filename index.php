<?php require_once('header.php') ?>

            <section class="site1-two">

                <div class="row">
                    <nav class="navbar navbar-expand-lg ">

                        <div class="center">
                            <div class="col-lg-1.5 "><a href="categories-view.php">ALL GATEGORIES</a></div>
                            <?php
									require_once("classes/categories.php");
                                    $cats = categories::get_all();
                                    
									foreach($cats as $cat)
									{
                                        $id = $cat['id'];
                                        ?>
                            <div class="col-lg-1.5">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $cat['name'] ?>
                                </a>
                                <div class="dropdown-menu p-0  w-100" style=" left: unset;"
                                    aria-labelledby="navbarDropdown">
                                    <div class="row">
                                        <?php
                                        
										require_once("classes/sub-categories.php");

								$scats = sub_categories::find_by_id($id);
								
								foreach($scats as $scat)
								{   
                                     $sc_id = $scat['id'];
                               
								?>
                                        <div class="col-6">
                                            <h5 class='text-left pl-4 pt-4'><?php echo $scat['sub_cat_name']?></h5>

                                            <?php
									require_once("classes/products.php");
                                    $products = products::find_by_id($sc_id);
                                    
									foreach($products as $product)
									{
                                       $id = $product['brand_id'];
                                        require_once("classes/brands.php");

                                        $brands = brands::find_by_id($id);
                                        
                                        foreach(array_unique($brands) as $brand)
                                        {   
   
                                        ?>
                                            <ul class='p-3'>
                                                <li> <a class="dropdown-item" href="#"><?php echo $brand['name']?></a>
                                                </li>
                                            </ul>


                                            <?php }?>
                                            <?php }?>
                                        </div>



                                        <?php }?>
                                    </div>

                                </div>
                            </div>
                            <?php } ?>



                        </div>
                    </nav>
                </div>







            </section>
            <!--section 3-->
            <section class="site1-three">
                <div class="row">
                    <div class="col-lg-7 float-left">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators ol-owl">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active li-owl">
                                </li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1" class=" li-owl"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=" li-owl"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100 owl-img" src="images/Screenshot (54).jpg"
                                        alt="First slide">
                                    <h2 class="owl-h2">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Accusantium
                                        officiis </h2>
                                    <p class="owl-p">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt
                                        aspernatur </p>
                                    <button class="owl-btn"> here</button>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 owl-img" src="images/Screenshot (54).jpg"
                                        alt="Second slide">
                                    <h2 class="owl-h2">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Accusantium
                                        officiis </h2>
                                    <p class="owl-p">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt
                                        aspernatur </p>
                                    <button class="owl-btn"> here</button>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 owl-img" src="images/Screenshot (54).jpg"
                                        alt="Third slide">
                                    <h2 class="owl-h2">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Accusantium
                                        officiis </h2>
                                    <p class="owl-p">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt
                                        aspernatur </p>
                                    <button class="owl-btn"> here</button>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                data-slide="prev">
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                data-slide="next">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-5  float-right">
                        <div class="col-lg-6 float-right ">
                           <img src="images/Home.jpg" class='product' alt="" srcset="">
                        </div>
                        <div class="col-lg-6 float-right">
                        <img src="images/Home.jpg" class='product' alt="" srcset="">

                        </div>
                        <div class="col-lg-6 float-right">
                        <img src="images/Home.jpg" class='product' alt="" srcset="">

                        </div>
                        <div class="col-lg-6 float-right">
                        <img src="images/Home.jpg" class='product' alt="" srcset="">

                        </div>
                        <div class="col-lg-6 float-right">
                        <img src="images/Home.jpg" class='product' alt="" srcset="">

                        </div>
                        <div class="col-lg-6 float-right">
                        <img src="images/Home.jpg" class='product' alt="" srcset="">

                        </div>
                    </div>
                </div>
            </section>
            <section class="small-nav">
                <div class="row row-site">
                    <div class="col-lg-2">
                        <div class="customes"><i class="fas fa-truck-moving"></i>
                            <p> delivary</p>
                            <span>here</span>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="customes"><i class="fas fa-truck-moving"></i>
                            <p> delivary</p>
                            <span>here</span>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="customes"><i class="fas fa-truck-moving"></i>
                            <p> delivary</p>
                            <span>here</span>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="customes"><i class="fas fa-truck-moving"></i>
                            <p> delivary</p>
                            <span>here</span>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="customes"><i class="fas fa-truck-moving"></i>
                            <p> delivary</p>
                            <span>here</span>
                        </div>
                    </div>


                </div>
            </section>
            <section class="feat-cate">
                <div class="title">
                    <h4>featured categories</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="one">
                    <?php
								require_once("classes/categories.php");
                                $cats = categories::rendom_select();
                                
                                foreach($cats as $cat)
                                {
                                    ?>
                    <div class="item">
                        <div class="photo-owl"><a href="http://#"><img src="<?php echo $cat['category_img'] ?>"
                                    alt=""></a></div>
                    </div>
                    <?php }?>

                </div>
            </section>

            <div class="blank">
                <p>Download our app today!Dont miss our mobile-only offers and shop with Android Play.</p>
            </div>

            <section class="feat-cate">
                <div class="title">
                    <h4>Hot New Arrivals</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="two">
                    <?php

                        $products= products::get_all();
                        foreach(array_slice($products, -5) as $product){
                         
                    
                    ?>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="<?php echo $product['product_images']?>" alt="">
                            <p><?php echo $product['shipping_price']?></p><span><?php echo $product['name']?></span><button><a href="ShoppingCart.php?id=<?php echo $product['id']?>"> Add to card</a></button>
                        </div>
                    </div>
                            <?php   }?>
               
                </div>
            </section>

            <section class="feat-cate sec">
                <div class="title">
                    <h4>Trendig Now</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="three">
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitorvidew &</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                </div>
            </section>
            <div class="offers-sec">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="offers-div">
                            <a href="#"><img src="images/ads.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="offers-div">
                            <a href="#"><img src="images/ads2.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>

            <section class="feat-cate ">

                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#Bestsellers" role="tab"
                            aria-controls="Bestsellers" aria-selected="true">Bestsellers</a>
                    </li>
                    <?php
										require_once("classes/sub-categories.php");

								$scats = sub_categories::get_all();
								
								foreach($scats as $scat)
								{   
    
								?>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab"
                            href="#<?php echo $scat['sub_cat_name']?>" role="tab"
                            aria-controls="<?php echo $scat['sub_cat_name'] ?>" aria-selected="false">
                            <?php echo $scat['sub_cat_name'] ?></a>
                    </li>
                    <?php }?>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Bestsellers" role="tabpanel"
                        aria-labelledby="Bestsellers-tab">

                        <div class="owl-carousel owl-theme four">
                            <?php
										

								$products = products::get_all();
								
								foreach($products as $product)
								{   
                                    if($product['qty']<5){
                               
								?>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="<?php echo $product['product_images']?>"
                                        alt="">
                                    <p><?php echo $product['shipping_price']?></p><span><?php echo $product['name']?></span><button><a href="ShoppingCart.php?id=<?php echo $product['id']?>"> Add to card</a></button>
                                </div>
                            </div>
                            <?php }}?>
                        </div>
                    </div>

                    <?php
                        
								$scats = sub_categories::get_all();
								
								foreach($scats as $scat)
								{   
                                    $sc_id = $scat['id'];

                               
							
                                $products = products::find_by_id($sc_id);
                                
                                foreach($products as $product)
                                {
                               
								?>

                    <div class="tab-pane fade" id="<?php echo $scat['sub_cat_name']?>" role="tabpanel"
                        aria-labelledby="<?php echo$scat['sub_cat_name'] ?>-tab">
                        <div class="owl-carousel owl-theme four">
                            <?php 
                            
                            $products = products::find_by_id($sc_id);
                                
                                foreach($products as $product)
                                {
                               
                            ?>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="<?php echo $product['product_images']?>"
                                        alt="">
                                    <p>$148</p><span>videw & air qulity monitor</span><button><a href="ShoppingCart.php?id=<?php echo $product['id']?>"> Add to card</a></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php }} ?>
                </div>





            </section>



            <section class="feat-cate sec">
                <div class="title">
                    <h4>Hot New Arrivals</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="five">
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                </div>
            </section>


            <section class="product2 ">
                <div class="row">
                    <div class="col-lg-2 product2-img"><img src="images/cam.png" alt="" style="width: 100%;"></div>
                    <div class="col-lg-10 ">
                        <div class="owl-carousel owl-theme" id="six">
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                    <p>$148</p><span>videw & air qulity monitorvidew &</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                    <p>$148</p><span>videw & air qulity monitor</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                    <p>$148</p><span>videw & air qulity monitor</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="feat-cate sec2 sec mt-4">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#Bestsellers2" role="tab"
                            aria-controls="Bestsellers2" aria-selected="true">Bestsellers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#Android-Phones2" role="tab"
                            aria-controls="Android-Phones2" aria-selected="false">Android Phones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ios-Phones2" role="tab"
                            aria-controls="ios-Phones2" aria-selected="false">ios Phones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tablets2" role="tab"
                            aria-controls="tablets2" aria-selected="false">tablets</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Bestsellers2" role="tabpanel"
                        aria-labelledby="Bestsellers-tab2">

                        <div class="owl-carousel owl-theme" id='seven'>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/images.png" alt="">
                                    <div class="dic">
                                        <p>$148</p><span>videw & air qulity monitor</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/images.png" alt="">
                                    <div class="dic">
                                        <p>$148</p><span>videw & air qulity monitor</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="photo-owl owl-hot"><img src="images/images.png" alt="">
                                    <div class="dic">
                                        <p>$148</p><span>videw & air qulity monitor</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

















                    <div class="tab-pane fade" id="Android-Phones2" role="tabpanel"
                        aria-labelledby="Android-Phones-tab2">
                        .........</div>
                    <div class="tab-pane fade" id="ios-Phones2" role="tabpanel" aria-labelledby="ios-Phones-tab2">
                        .............................</div>
                    <div class="tab-pane fade" id="tablets2" role="tabpanel" aria-labelledby="tablets-tab2">
                        ...................</div>
                </div>
            </section>









            <section class="feat-cate brands-sec">
                <div class="title">
                    <h4>BRANDS</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="brands">
                <?php 
                        $brands=brands::get_all();
                        foreach($brands as $brand){
                ?>
                    <div class="item">
                        <div class="photo-owl"><a href="http://#"><img src="<?php echo $brand['logo']?>" alt=""></a>
                        </div>
                    </div>
                        <?php }?>
                </div>
            </section>

        </div>
        <?php require_once('footer.php') ?>