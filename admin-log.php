<?php 
    session_start();

?>
<DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">

        <link rel="stylesheet" href="css/style.css">


    </head>

    <body>
        <div class="top-nav">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Pricing</a>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                    </span>
                </div>
            </nav>
        </div>

        <div class="container-site">


            <section class="site1-one">

                <nav class="navbar navbar-expand-lg ">
                    <div class="collapse-w3">
                        <div class="w3-sidebar w3-bar-block w3-collapse w3-card" style="width:200px;" id="mySidebar">
                            <button class="w3-bar-item w3-button w3-hide-large" onclick="w3_close()">Close
                                &times;</button>
                            <div class="w3-sidebar w3-bar-block">
                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 2</a>
                                <div class="w3-dropdown-hover">
                                    <button class="w3-button">COMUTERS & LAPTOPS <i
                                            class="fa fa-caret-down"></i></button>
                                    <div class="w3-dropdown-content w3-bar-block">
                                        <ul>
                                            <li><a href="#">Laptops,Ultrabooks & Computers</a></li>
                                            <li><a href="#">camiras & photography</a></li>
                                            <li><a href="#">smart phones & Tablets</a></li>
                                            <li><a href="#">Video Games & Consoles</a></li>
                                            <li><a href="#">Tv & Audio</a></li>
                                            <li><a href="#">Gadgets</a></li>
                                            <li><a href="#">Car Elctronic & GPS</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="#" class="w3-bar-item w3-button">Link 3</a>
                            </div>
                        </div>

                        <button class="w3-button " onclick="w3_open()">&#9776;</button>
                    </div>
                    <a class="navbar-brand mr-auto" href="#"><img src="images/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="search-mob collapse-w3">
                        <button id="search-btn"><i class="fas fa-search"></i></button>
                        <div class="search position-absolute " id="search">
                            <form style="display: flex;">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter email">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>

                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item dropdown" style="margin: auto;">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <div class="w-25">
                            <ul class="navbar-nav mr-auto d-flex justify-content-around align-items-center">
                                <li class="nav-item d-flex justify-content-around align-items-center">
                                    <i class="fas fa-retweet"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="far fa-heart"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item">
                                    <span class="d-block">your Cart</span>
                                    <span>$15410</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>


        </div>
        <hr class="m-auto">

        <div class="container-site">

        <section class="sec-login w-50 ml-auto mr-auto">

          
                    <form action="login_action.php" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" placeholder="Enter email" name='email'>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" name="pass" placeholder="Password">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">remmber me</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
         

        </section>


    </div>









        <footer>
            <div class="top-footer">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="sing-up row">
                            <div class="content-fo col-lg-6">
                                <div> <i class="far fa-paper-plane"></i></div>
                                <div>
                                    <h3>sing up for newsletter</h3>
                                    <p>..and receive $20 coipon for first shopping.</p>
                                </div>
                            </div>
                            <div class="form col-lg-6">
                                <form style="display: flex;">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Enter email">
                                        <button type="submit" class="btn btn-primary">Submit</button>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 right-links">
                        <a href="http://" target="_blank" class="fab fa-facebook-f" rel="noopener noreferrer">
                            Facebook</a>
                        <a href="http://" target="_blank" class="fab fa-twitter" rel="noopener noreferrer"> Twitter</a>
                        <a href="http://" target="_blank" class="fab fa-google-plus-g" rel="noopener noreferrer">
                            google+</a>
                        <a href="http://" target="_blank" class="fas fa-rss" rel="noopener noreferrer"> Rss</a>
                        <a href="http://" target="_blank" class="fab fa-instagram" rel="noopener noreferrer">
                            Instagram</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="buttom-fo pl-5">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ml-4"><img src="images/logo.png" alt=""
                                style="    padding-bottom: 50px;margin-left: 53px;"></div>

                        <div class="row">
                            <div class="col-lg-6 text-center">
                                <div class="d-flex  justify-content-center">
                                    <div> <i class="far fa-paper-plane"></i></div>
                                    <div class="text-left">
                                        <p class=" m-auto font-weight-bold">Got Qustion?Call us</p>
                                        <p class="font-weight-bold mt-4 h3 "> 801017197</p>
                                        <p class="text-left">Lorem, ipsum dolor sit amet consectetur<br> adipisicing
                                            elit. A qu</p>
                                        <p class="text-left mt-5"><a href="#" class="fas fa-location-arrow"> find in
                                                map</a></p>
                                    </div>

                                </div>

                            </div>
                            <div class="col-lg-6 text-center">
                                <div class="d-flex  justify-content-center">
                                    <div> <i class="fas fa-hand-holding-usd"></i></div>
                                    <div class="text-left">
                                        <p class=" m-auto font-weight-bold">WE USING SAFE PAYMENTS</p>
                                        <img width="60%" src="images/payment.png" alt="">
                                        <p class="text-left m-auto">secrured by:</p>
                                        <img width="60%" src="images/payment.png" alt="">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4">
                                <ul class="mt-5  list h5 p-0 pt-4" style="line-height: 2.2rem;">
                                    <li class=" font-weight-bold d-block mb-4">FIND IT FAST</li>
                                    <li><a href="#">Laptops,Ultrabooks & Computers</a></li>
                                    <li><a href="#">camiras & photography</a></li>
                                    <li><a href="#">smart phones & Tablets</a></li>
                                    <li><a href="#">Video Games & Consoles</a></li>
                                    <li><a href="#">Tv & Audio</a></li>
                                    <li><a href="#">Gadgets</a></li>
                                    <li><a href="#">Car Elctronic & GPS</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-4 d-flex" style="align-items: center;">
                                <ul class="mt-5  list h5 p-0 pt-4" style="line-height: 2.2rem;">

                                    <li><a href="#">Video Games & Consoles</a></li>
                                    <li><a href="#">Tv & Audio</a></li>
                                    <li><a href="#">Gadgets</a></li>
                                    <li><a href="#">Car Elctronic & GPS</a></li>
                                </ul>
                            </div>

                            <div class="col-lg-4 border-left">
                                <ul class="mt-5  list h5 p-0 pt-4" style="line-height: 2.2rem;">
                                    <li class=" font-weight-bold d-block mb-4">CUSTOMER CARE</li>
                                    <li><a href="#">Laptops,Ultrabooks & Computers</a></li>
                                    <li><a href="#">camiras & photography</a></li>
                                    <li><a href="#">smart phones & Tablets</a></li>
                                    <li><a href="#">Video Games & Consoles</a></li>
                                    <li><a href="#">Tv & Audio</a></li>
                                    <li><a href="#">Gadgets</a></li>
                                    <li><a href="#">Car Elctronic & GPS</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="last">
                <div>
                    <p> Copyright 2017</p>
                </div>
                <div>
                    <p> made by me </p>
                </div>
            </div>

        </footer>


        <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script src="js/aos.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/bootstrap.js"></script>

        <script src="js/js.js"></script>
    </body>

    </html>