<?php
session_start();

require_once("classes/products.php");

if (empty($_SESSION["cart_products"])) $_SESSION["cart_products"] =[];
if(!empty($_GET['id'])){ 
   $cart_arr = $_SESSION["cart_products"];
   $product_obj = products::get_by_session($_GET['id']);
 $cart_arr[$_GET['id']] = ["qty"=>"1" , "product"=>  $product_obj ];
 //var_dump(  $cart_arr[$_GET['id']]);
 $_SESSION["cart_products"]=$cart_arr;
   //$_SESSION["cart_products"] = array_unique($cart_arr);
};



?>
<DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">

        <link rel="stylesheet" href="css/style.css">


    </head>

    <body>
        <div class="top-nav"><?php
// var_dump($item)
?>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Pricing</a>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                    </span>
                </div>
            </nav>
        </div>

        <div class="container-site">


            <section class="site1-one">

                <nav class="navbar navbar-expand-lg ">
                    <div class="collapse-w3">
                        <div class="w3-sidebar w3-bar-block w3-collapse w3-card" style="width:200px;" id="mySidebar">
                            <button class="w3-bar-item w3-button w3-hide-large" onclick="w3_close()">Close
                                &times;</button>
                            <div class="w3-sidebar w3-bar-block">
                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 2</a>
                                <?php
									require_once("classes/categories.php");
                                    $cats = categories::get_all();
                                    
									foreach($cats as $cat)
									{
                                        $id = $cat['id'];
                                        ?>
                                <div class="w3-dropdown-hover">
                                    <button class="w3-button"> <?php echo $cat['name'] ?><i
                                            class="fa fa-caret-down"></i></button>
                                    <div class="w3-dropdown-content w3-bar-block">
                                        <ul>
                                            <?php
										require_once("classes/sub-categories.php");

								$scats = sub_categories::find_by_id($id);
								
								foreach($scats as $scat)
								{
								?>
                                            <li><a href="#"><?php echo $scat['sub_cat_name']?></a></li>
                                            <?php } ?>


                                        </ul>
                                    </div>
                                </div>
                                <?php } ?>
                                <a href="#" class="w3-bar-item w3-button">Link 3</a>
                            </div>
                        </div>

                        <button class="w3-button " onclick="w3_open()">&#9776;</button>
                    </div>
                    <a class="navbar-brand mr-auto" href="#"><img src="images/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="search-mob collapse-w3">
                        <button id="search-btn"><i class="fas fa-search"></i></button>
                        <div class="search position-absolute " id="search">
                            <form style="display: flex;">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter email">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>

                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item dropdown" style="margin: auto;">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <div class="w-25">
                            <ul class="navbar-nav mr-auto d-flex justify-content-around align-items-center">
                                <li class="nav-item d-flex justify-content-around align-items-center">
                                    <i class="fas fa-retweet"></i>
                                    
                                </li>
                                <?php
                                 require_once("classes/products.php");
                             
                               
                                // (!empty($ids)? $idp=$ids:null) ;
                              
                                 
                                //  $products =  products::get_by_session($idp);
                                
                                 
                                //  echo $idp
                                 ?>
                                <li class="nav-item dropdown" >
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-shopping-bag"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <?php
                             if (count($_SESSION["cart_products"])>0)
                             {
                                foreach(array_unique($_SESSION['cart_products']) as  $prod_id){ 
                                   $product_obj = products::get_by_session($prod_id);
                                   ?>
                                    <a class="dropdown-item" href="#"><?php  echo $product_obj->name ?></a>
                                    <?php
                                }
                             }
                             
                           ?>
                               
                                </div>
                            </li>
                             
                            
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="far fa-heart"></i>
                                    
                                </li>
                                <li class="nav-item">
                                    <span class="d-block"><?php echo (!empty($_SESSION['customer']) ?  $_SESSION['customer']:'<a href="registration.php">login</a>')?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>