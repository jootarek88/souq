<?php
    session_start();
    if (empty($_SESSION['admin']))
    {
        header("location:admin-log.php?msg=secure_page");
    }
    
?>
<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="css/control.css" >
<body class="DASHBOURD">

<div class="w3-sidebar w3-bar-block w3-collapse w3-card w3-animate-left" style="width:200px;" id="mySidebar">
  <button class="w3-bar-item w3-button w3-large w3-hide-large" onclick="w3_close()">Close &times;</button>
  <a href="users.php" class="w3-bar-item w3-button">Users</a>
  <a href="categories.php" class="w3-bar-item w3-button">categories</a>
  <a href="sub-categories.php" class="w3-bar-item w3-button">sub_categories</a>
  <a href="customers.php" class="w3-bar-item w3-button">customers</a>
  <a href="brands.php" class="w3-bar-item w3-button">Brands</a>
  <a href="products.php" class="w3-bar-item w3-button">products</a>
  <a href="order.php" class="w3-bar-item w3-button">orders</a>
</div>

<div class="w3-main" style="margin-left:200px">
<div class="w3-teal">
  <button class="w3-button w3-teal w3-xlarge w3-hide-large" onclick="w3_open()">&#9776;</button>
  <div class=" d-flex w3-container">
    <h1>DASHBOURD</h1>
    
  
 
  </div>
</div>




