<DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">

        <link rel="stylesheet" href="css/style.css">


    </head>

    <body>
        <div class="top-nav">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Pricing</a>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                    </span>
                </div>
            </nav>
        </div>

        <div class="container-site">


            <section class="site1-one">

                <nav class="navbar navbar-expand-lg ">
                    <div class="collapse-w3">
                        <div class="w3-sidebar w3-bar-block w3-collapse w3-card" style="width:200px;" id="mySidebar">
                            <button class="w3-bar-item w3-button w3-hide-large" onclick="w3_close()">Close
                                &times;</button>
                            <div class="w3-sidebar w3-bar-block">
                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 2</a>
                                <?php
                                	require_once("classes/categories.php");
                                    $cats = categories::get_all();
                                    
									foreach($cats as $cat)
									{
                                        $id = $cat['id'];
                                
                                ?>
                                <div class="w3-dropdown-hover">
                                    <button class="w3-button">COMUTERS & LAPTOPS <i
                                            class="fa fa-caret-down"></i></button>
                                    <div class="w3-dropdown-content w3-bar-block">
                                        <ul>
                                            <?php
                                        require_once("classes/sub-categories.php");

                                                $scats = sub_categories::find_by_id($id);

                                                foreach($scats as $scat)
                                                {   
                                                    ?>
                                            <li><a href="#"><?php $scat['sub_cat_name'] ?></a></li>

                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php }?>
                                <a href="#" class="w3-bar-item w3-button">Link 3</a>
                            </div>
                        </div>

                        <button class="w3-button " onclick="w3_open()">&#9776;</button>
                    </div>
                    <a class="navbar-brand mr-auto" href="#"><img src="images/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="search-mob collapse-w3">
                        <button id="search-btn"><i class="fas fa-search"></i></button>
                        <div class="search position-absolute " id="search">
                            <form style="display: flex;">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter email">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>

                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item dropdown" style="margin: auto;">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                        <div class="w-25">
                            <ul class="navbar-nav mr-auto d-flex justify-content-around align-items-center">
                                <li class="nav-item d-flex justify-content-around align-items-center">
                                    <i class="fas fa-retweet"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item d-flex justify-content-around">
                                    <i class="far fa-heart"></i>
                                    <span>10</span>
                                </li>
                                <li class="nav-item">
                                    <span class="d-block">your Cart</span>
                                    <span>$15410</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>


        </div>
        <hr class="m-auto">

        <div class="container-site">
            <section class="cat-product">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="side-menu w-75">
                            <div class="w3-sidebar w3-bar-block">

                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 1</a>
                                <a href="#" class="w3-bar-item w3-button">Link 2</a>
                                <?php
                                	require_once("classes/categories.php");
                                    $cats = categories::get_all();
                                    
									foreach($cats as $cat)
									{
                                        $id = $cat['id'];
                                
                                ?>
                                <div class="w3-dropdown-hover">
                                    <button class="w3-button"><?php echo $cat['name'] ?> <i
                                            class="fa fa-caret-down"></i></button>
                                    <div class="w3-dropdown-content w3-bar-block">
                                    
                                        <ul>
                                        <?php
                                        require_once("classes/sub-categories.php");

                                                $scats = sub_categories::find_by_id($id);

                                                foreach($scats as $scat)
                                                {   
                                                    ?>
                                            <li><a href="#"><?php echo $scat['sub_cat_name'] ?></a></li>
                                     
                                                <?php }?>
                                        </ul>
                                    </div>
                                </div>
                                <?php }?>

                                <a href="#" class="w3-bar-item w3-button">Link 3</a>
                            </div>
                            <div class="side-ads"><img src="images/ads2.jpg" alt=""></div>
                            <div class="side-ads"><img src="images/ads2.jpg" alt=""></div>
                            <div class="side-ads"><img src="images/ads2.jpg" alt=""></div>
                        </div>


                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                        <?php 
                          require_once("classes/products.php");

                          $products = products::rendom_select();

                          foreach($products as $product)
                          {   
                        ?>
                            <div class="col-lg-3"><a href="#"><img src="<?php echo $product['product_images'] ?>" alt=""></a></div>
                          <?php } ?>
                        </div>
                        <section>
                            <div class="title">
                                <h4>Trendig Now</h4>
                            </div>
                            <hr>

                            <div class="owl-carousel owl-theme" id="cat-one">
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitorvidew &</span><button>Add to
                                            card</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="feat-cate sec">
                            <div class="title">
                                <h4>Trendig Now</h4>
                            </div>
                            <hr>

                            <div class="owl-carousel owl-theme" id="cat-two">
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitorvidew &</span><button>Add to
                                            card</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                                        <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>

            <section class="feat-cate sec">
                <div class="title">
                    <h4>Trendig Now</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="three">
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitorvidew &</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="photo-owl owl-hot"><img src="images/cam.png" alt="">
                            <p>$148</p><span>videw & air qulity monitor</span><button>Add to card</button>
                        </div>
                    </div>
                </div>
            </section>

            <section class="feat-cate brands-sec">
                <div class="title">
                    <h4>featured categories</h4>
                </div>
                <hr>

                <div class="owl-carousel owl-theme" id="brands">
                <?php 
                require_once('classes/brands.php');
                        $brands=brands::get_all();
                        foreach($brands as $brand){
                ?>
                    <div class="item">
                        <div class="photo-owl"><a href="http://#"><img src="<?php echo $brand['logo'] ?>" alt=""></a></div>
                    </div>
                        <?php }?>

                </div>
            </section>


        </div>








<?php require_once('footer.php') ?>