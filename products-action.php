<?php


if (!empty($_POST['name'])&&!empty($_POST['description'])&&!empty($_POST['unit_price'])&&!empty($_POST['qty'])&&!empty($_POST['vendor_id'])){

    $name = $_POST['name'];
    $description = $_POST['description'];
    $unit_price = $_POST['unit_price'];
    $shipping_price = $_POST['shipping_price'];
    $qty = $_POST['qty'];
    $status = $_POST['status'];
    $vendor_id = $_POST['vendor_id'];
    $sub_category_id = $_POST['sub_category_id'];
    $category_id = $_POST['category_id'];
    $brand_id = $_POST['brand_id'];
    if (!empty($_FILES['product_images']['name'])){        
        $product_images =  'images/brands/'.date('dmYHIs').'.'.pathinfo($_FILES['product_images']['name'] , PATHINFO_EXTENSION);
    
        move_uploaded_file($_FILES['product_images']["tmp_name"] , $product_images);
    }else{
        $product_images = 'images/brands/default.png';
    }
    require_once('classes/products.php');
    $products = new products();
    $products->name = $name ;
    $products->description = $description ;
    $products->unit_price = $unit_price ;
    $products->shipping_price = $shipping_price ;
    $products->qty = $qty ;
    $products->status = $status ;
    $products->vendor_id = $vendor_id ;
    $products->sub_category_id = $sub_category_id ;
    $products->category_id = $category_id ;
    $products->create_at= date(dmyhis);
    $products->brand_id = $brand_id ;
    $products->product_images = $product_images ;

    $products->add();
     header('location:products.php'); 
 }

if(!empty($_GET['id'])){
    $id = $_GET['id'];
    require_once('classes/products.php');
    $b = new  products();
    $b->id = $id ;
    $b->delete();
    header('location:products.php'); 


}