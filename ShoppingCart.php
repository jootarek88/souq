<?php require_once('header.php') ?>


<section class='sc'>


    <div class="row">
        <div class="col-lg-9">
        <table class="table">
            <thead class="thead-dark">
                <tr>    
                    <th scope="col">description</th>
                    <th scope="col">shipping_price</th>
                    <th scope="col">qty</th>
                    <th scope="col">delete</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $sum=0;
                $qty=0;
   require_once("classes/products.php");

            foreach($_SESSION['cart_products'] as $key=> $prod_arr){                 
                $sum+= $prod_arr["product"]->shipping_price * $prod_arr["qty"];  
                $qty+= $prod_arr["qty"]       ;
                ?>
            

                <tr>
                    <th scope="row"><img src="<?php echo  $prod_arr["product"]->product_images ?>" alt=""><?php echo  $prod_arr["product"]->name ?> </th>
                    <td><?php echo $prod_arr["product"]->shipping_price?></td>
                 
                    <td><a  href="ShoppingCart-action.php?value=1&id=<?php echo $prod_arr["product"]->id ?>">+</a>  <?php  echo $prod_arr["qty"];   ?> 
                    <a  href="ShoppingCart-action.php?value=-1&id=<?php echo $prod_arr["product"]->id ?>" >-</a></td>


                    <td><a href="delete.php?id=<?php echo  $prod_arr["product"]->id ?>">delete</a></td>
             
                </tr>
                <?php  }?>
            </tbody>
        </table>
        </div>
        <div class="col-lg-3">
            <div class="order-summary">
                <h2>Order Summary</h2>
                <hr>
                <div class="sum d-flex justify-content-between">
                    <div class="l-sum">
                        <p>qty of products</p>
                        <p>shipping</p>
                        <p>vat tax</p>
                    </div>
                    <div class="r-sum">
                        <p><?php echo $qty ?></p>
                        <p>shipping</p>
                        <p>vat tax</p>
                    </div>
                </div>
                <hr>
                <div class="total sum d-flex justify-content-between">
                    <div class="l-sum">
                        <p>total</p>
                    </div>
                    <div class="r-sum">
                        <p><?php  echo  $sum ?></p>
                    </div>
                </div>
                <div class="check text-center">
                <span>i have promo code</span>

                <button type="button" class="btn btn-primary btn-lg d-block m-auto">Large button</button>
                <a href="./">back to shop</a>
                </div>
                <hr>
                <div class='role-offer'>
                <ul>
                <li>30 day for free return</li>
                <li>30 day for free return</li>
                <li>30 day for free return</li>
                </ul>
                </div>
            </div>
        </div>
    </div>

</section>










</div ><!-- end-container div -->
<?php require_once('footer.php') ?>